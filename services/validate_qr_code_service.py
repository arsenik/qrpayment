def validate_get_params(params):
    if "amount" not in params:
        params["amount"] = 0
    if "account" not in params and "biller_id" not in params:
        raise Exception("Account or Biller_id is mandatory")
    return True
