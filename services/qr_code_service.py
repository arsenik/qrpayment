import re
import libscrc

ID_PAYLOAD_FORMAT = '00'
ID_POI_METHOD = '01'
ID_MERCHANT_INFORMATION_BOT = '29'
ID_SHOP_INFORMATION_BOT = '30'
ID_TRANSACTION_CURRENCY = '53'
ID_TRANSACTION_AMOUNT = '54'
ID_COUNTRY_CODE = '58'
ID_CRC = '63'
PAYLOAD_FORMAT_EMV_QRCPS_MERCHANT_PRESENTED_MODE = '01'
POI_METHOD_STATIC = '11'
POI_METHOD_DYNAMIC = '12'
MERCHANT_INFORMATION_TEMPLATE_ID_GUID = '00'
BOT_ID_MERCHANT_PHONE_NUMBER = '01'
BOT_ID_MERCHANT_TAX_ID = '02'
BOT_ID_MERCHANT_EWALLET_ID = '03'
GUID_PROMPTPAY = 'A000000677010111'
GUID_PROMPTPAY_2 = 'A000000677010112'
TRANSACTION_CURRENCY_THB = '764'
COUNTRY_CODE_TH = 'TH'
SHOP_REFERENCE = '622007'


def generatePayload(options):
    is_shop = options['account'] is None
    amount = options['amount']
    if is_shop:
        biller_id = options['biller_id']
        shop_id = options['shop_id']
        shop_ref = options['shop_reference']
        reference = options['reference']
    else:
        target = sanitize_target(options['account'])
        if len(target) >= 15:
            target_type = BOT_ID_MERCHANT_EWALLET_ID
        elif len(target) >= 13:
            target_type = BOT_ID_MERCHANT_TAX_ID
        else:
            target_type = BOT_ID_MERCHANT_PHONE_NUMBER

    data = [
        f(ID_PAYLOAD_FORMAT, PAYLOAD_FORMAT_EMV_QRCPS_MERCHANT_PRESENTED_MODE),
        f(ID_POI_METHOD, POI_METHOD_DYNAMIC if amount > 0 else POI_METHOD_STATIC),
        get_shop_data(biller_id, shop_id, shop_ref) if is_shop else get_target_data(target, target_type),
        f(ID_COUNTRY_CODE, COUNTRY_CODE_TH),
        f(ID_TRANSACTION_CURRENCY, TRANSACTION_CURRENCY_THB),
        f(SHOP_REFERENCE, reference) if is_shop else '',
        f(ID_TRANSACTION_AMOUNT, format_amount(amount)) if amount > 0 else ''
    ]
    data_to_crc = serialize(data) + ID_CRC + '04'
    data.append(f(ID_CRC, format_crc(libscrc.ccitt_false(bytes(data_to_crc, "ascii")))))
    return serialize(data)


def f(target_id, value):
    return ''.join([target_id, ('00' + str(len(value)))[-2:], value])


def get_target_data(target, target_type):
    return f(ID_MERCHANT_INFORMATION_BOT, serialize(
            [f(MERCHANT_INFORMATION_TEMPLATE_ID_GUID, GUID_PROMPTPAY), f(target_type, format_target(target))]))


def get_shop_data(biller_id, shop_id, shop_ref):
    return f(ID_SHOP_INFORMATION_BOT, serialize(
            [f(MERCHANT_INFORMATION_TEMPLATE_ID_GUID, GUID_PROMPTPAY_2), f('01', biller_id), f('02', shop_id),
             f('03', shop_ref)]))


def serialize(xs):
    return ''.join(xs)


def sanitize_target(target_id):
    return re.sub(r'[^0-9]', '', target_id)


def format_target(target_id):
    numbers = sanitize_target(target_id)
    if len(numbers) >= 13:
        return numbers
    return ('0000000000000' + re.sub(r'^0', '66', numbers))[-13:]


def format_amount(amount):
    return "{0:.2f}".format(amount)


def format_crc(crc_value):
    return ('0000' + hex(crc_value)[2:]).upper()[-4:]
