from database import db


def validate_transactions(transactions):
    mandatory_fields = ["payeeProxyId", "payeeProxyType", "payeeAccountNumber", "payerAccountNumber",
                        "payerAccountName", "payerName", "sendingBankCode", "receivingBankCode", "amount",
                        "transactionId", "transactionDateandTime", "billPaymentRef1", "billPaymentRef2",
                        "billPaymentRef3", "currencyCode", "channelCode", "transactionType"]
    for field in mandatory_fields:
        if field not in transactions:
            raise Exception("Missing mandatory field in transaction: " + field)
        # Check if already in db
        collections = db["transactions"]
        nb_docs = collections.count_documents({"transactionId": transactions["transactionId"]})
        if nb_docs > 0:
            raise Exception("Transaction id already exists")
    return True


def generate_response(transaction):
    return {
        "resCode": "00",
        "resDesc ": "success",
        "transactionId": transaction["transactionId"]
    }


def validate_get_params(params):
    mandatory_fields = ["amount", "payerAccountNumber", "payeeAccountNumber", "transactionDateandTime"]
    for field in mandatory_fields:
        if field not in params:
            raise Exception(field + " is missing from parameters")
    return True
