import pandas as pd
import io


def generate_gherkins(csv):
    buffer = io.StringIO(csv)
    df = pd.read_csv(buffer)
    table = ['| ' for _ in range(len(df) + 1)]
    for k, v in df.loc[:, :].items():
        le = df[k].map(len).max()
        table[0] += ' ' + k.ljust(le) + ' |'
        for i, val in enumerate(v):
            table[i + 1] += ' ' + val.ljust(le) + ' |'
    return '\n'.join(table)


def generate_csv(ger):
    rows = [[r.strip() for r in l.split('|') if r != ''] for l in ger.split('\n')]
    data = {
        rows[0][i]: [r[i] for r in rows[1:]] for i in range(0, len(rows[0]))
    }
    df = pd.DataFrame(data, columns=rows[0])
    return df.to_csv(index=False)
