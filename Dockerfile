FROM python:3

COPY requirements.txt /
RUN pip install --requirement requirements.txt
COPY app.py /
COPY database.py /
COPY api /api
COPY services /services

CMD [ "gunicorn", "-b 0.0.0.0:80", "app:app" ]
