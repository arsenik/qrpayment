from wsgiref.simple_server import make_server
import falcon
import os
from api.qr_code import QRCode
from api.transactions import Transactions
from api.gherkin import Gherkin
from api.to_csv import ToCsv

app = falcon.API()

app.add_route('/api/qrcode', QRCode())
app.add_route('/api/transactions', Transactions())
app.add_route('/api/gherkin', Gherkin())
app.add_route('/api/to_csv', ToCsv())

if __name__ == '__main__':
    with make_server('', int(os.environ.get('PORT', '5000')), app) as httpd:
        print('Serving on port {}...'.format(int(os.environ.get('PORT', '5000'))))

        # Serve until process is killed
        httpd.serve_forever()
