import falcon
import json
from services import gherkin_service


class ToCsv:

    def on_post(self, req, resp):
        resp.content_type = falcon.MEDIA_TEXT
        transaction_json = json.load(req.bounded_stream)
        ger = transaction_json['gherkin']
        resp.body = gherkin_service.generate_csv(ger)