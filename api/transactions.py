from database import db
from services.validate_transactions_service import validate_transactions, generate_response, validate_get_params
import falcon
import json
from dateutil.parser import parse
from bson.json_util import dumps


class Transactions:
    def on_get(self, req, resp):
        resp.content_type = falcon.MEDIA_JSON
        searched = {}
        try:
            is_valid = validate_get_params(req.params)
        except Exception as e:
            print(e)
            raise falcon.HTTPBadRequest("Error occurred", str(e))
        for key, value in req.params.items():
            if key == "amount":
                try:
                    searched["amount"] = float(value)
                except (ValueError, TypeError) as e:
                    raise falcon.HTTPBadRequest("Amount must be a number", e)
            elif key == "payeeAccountNumber":
                searched["payeeAccountNumber"] = value
            elif key == "payerAccountNumber":
                searched["payerAccountNumber"] = value
            elif key == "payerName":
                searched["payerName"] = value
            elif key == "transactionDateandTime":
                searched["transactionDateandTime"] = {"$gte": parse(value)}
        trans = {}
        if is_valid:
            collections = db["transactions"]
            trans = collections.find_one(searched)
        resp.body = dumps(trans)

    def on_post(self, req, resp):
        resp.content_type = falcon.MEDIA_JSON
        transaction_json = json.load(req.bounded_stream)
        try:
            is_valid = validate_transactions(transaction_json)
        except Exception as e:
            print(e)
            raise falcon.HTTPBadRequest("Error occurred", str(e))
        # Format amount to float
        try:
            transaction_json["amount"] = float(transaction_json["amount"])
        except (ValueError, TypeError):
            is_valid = False
        # Format date
        try:
            transaction_json["transactionDateandTime"] = parse(transaction_json["transactionDateandTime"])
        except (ValueError, TypeError):
            is_valid = False
        if is_valid:
            collections = db["transactions"]
            collections.insert_one(transaction_json)
            resp.body = json.dumps(generate_response(transaction_json))
        else:
            resp.body = json.dumps({"result": "KO"})
