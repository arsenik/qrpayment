import falcon
import json
from services import gherkin_service


class Gherkin:

    def on_post(self, req, resp):
        resp.content_type = falcon.MEDIA_TEXT
        transaction_json = json.load(req.bounded_stream)
        csv = transaction_json['csv']
        resp.body = gherkin_service.generate_gherkins(csv)
