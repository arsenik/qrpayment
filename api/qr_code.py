import falcon
import logging
import qrcode
import io
from services import qr_code_service, validate_qr_code_service


class QRCode:
    def on_get(self, req, resp):
        """Handles GET requests"""
        account = None
        biller_id = None
        shop_id = None
        shop_reference = None
        reference = None
        try:
            validate_qr_code_service.validate_get_params(req.params)
        except Exception as e:
            raise falcon.HTTPBadRequest("An error occurred", str(e))
        for key, value in req.params.items():
            if key == "account":
                account = value
            elif key == "amount":
                amount = value
            elif key == "biller_id":
                biller_id = value
            elif key == "shop_id":
                shop_id = value
            elif key == "shop_reference":
                shop_reference = value
            elif key == "reference":
                reference = value
        try:
            amount = float(amount)
        except (ValueError, TypeError) as e:
            raise falcon.HTTPBadRequest("Amount must be a number", e)
        logging.info("Start QR code generation for account %s and amount %f" % (account, amount))
        checksum = qr_code_service.generatePayload(
            {'account': account, 'amount': amount, 'biller_id': biller_id, 'shop_id': shop_id,
             'shop_reference': shop_reference, 'reference': reference})
        logging.info("Checksum: %s" % checksum)
        img = qrcode.make(checksum)

        buffer = io.BytesIO()
        img.save(buffer, 'PNG')
        resp.status = falcon.HTTP_200  # This is the default status
        resp.content_type = falcon.MEDIA_PNG
        resp.data = buffer.getvalue()
        return resp
